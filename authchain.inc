<?php
/**
 * @file
 * AuthChain implementation.
 */

/** Pattern for authentication error. */
define('AUTHCHAIN_MESSAGE_AUTH_ERROR', 'Error authenticating "@service": @message');

/**
 * Authentication context type.
 */
abstract class AuthChainContext extends AuthChainPlugin {
  /**
   * Construct the context.
   */
  public function __construct() {
    $this->establish();
  }

  /**
   * Initialize and stablish context.
   */
  protected abstract function establish();
}

/**
 * Authentication service provider.
 */
abstract class AuthChainService extends AuthChainPlugin {
  /**
   * Setting values array.
   * @var array
   */
  protected $settings;

  /**
   * Initialize plugin.
   *
   * @param array $settings
   *   Plugin settings, or NULL to use default values.
   */
  public function __construct($settings = NULL) {
    if (!isset($settings)) {
      $settings = $this->getDefaultSettings();
    }
    $this->settings = $settings;
  }

  /**
   * Get default settings.
   *
   * @return array
   *   Default setting values.
   */
  public function getDefaultSettings() {
    return array();
  }

  /**
   * Get settings form.
   *
   * @param array $settings
   *   Default or saved settings.
   * @return array
   *   FAPI array.
   */
  public function getSettingsForm() {
    return array();
  }

  /**
   * Get the class name of a supported authentication context.
   *
   * @return string
   *   Context plugin class name.
   */
  public abstract function getSupportedContext();

  /**
   * Authenticate against this service.
   */
  public abstract function authenticate();
}

/**
 * Generate exception.
 *
 * @param string $name
 *   Name of component generating exception.
 * @param string $message
 *   Extra message text.
 * @param $class
 *   Exception class. Defaults to AuthChainException.
 *
 * @return
 *   Generated exception, of the given $class.
 */
function authchain_exception($name, $message, $class = 'AuthChainException') {
  // Construct the exception and return.
  return new $class($name, $message);
}

/**
 * General AuthChain exception class.
 */
class AuthChainException extends Exception {
  /**
   * Component name.
   * @var name
   */
  protected $name;

  /**
   * Construct execution exception.
   */
  public function __construct($name, $message) {
    parent::__construct($message);

    $this->name = $name;
  }

  /**
   * Get component name for this exception.
   *
   * @return string
   *   Component name.
   */
  public function getName() {
    return $this->name;
  }
}
