<?php
/**
 * @file
 * Authentication chain.
 */

/**
 * Implements hook_permission().
 */
function authchain_permission() {
  return array(
    'administer authchain' => array(
      'title' => t('Administer AuthChain'),
      'description' => t('Administer chained authentication modules.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function authchain_menu() {
  $items = array();

  $items['admin/config/people/authchain'] = array(
    'title' => 'AuthChain settings',
    'description' => 'Manage configuration for chained authentication.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('authchain_admin_overview_form'),
    'access arguments' => array('administer authchain'),
    'file' => 'authchain.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/people/authchain/%authchain_service'] = array(
    'title' => 'Service settings',
    'description' => 'Configure authentication service.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('authchain_admin_service_settings_form', 4),
    'access arguments' => array('administer authchain'),
    'file' => 'authchain.admin.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Load callback for service.
 */
function authchain_service_load($name) {
  $info = authchain_get_info('service');
  if (isset($info[$name]) && class_exists($info[$name]['class'])) {
    // Fully load service with settings.
    $service_info = $info[$name];
    $result = db_select('authchain', 'ac')
      ->fields('ac', 'settings')
      ->condition('ac.service', $name)
      ->execute();
    if ($row = $result->fetchObject()) {
      // Use saved settings.
      $service_info['settings'] = $row->settings;
    }
    else {
      // Use default settings.
      $service_info['settings'] = NULL;
    }

    // Initialize service and return.
    $service = new $service_info['class']($service_info['settings']);
    return $service;
  }
}

/**
 * Fetch AuthChain implementation info.
 */
function authchain_get_info($type, $reset = FALSE) {
  $info = &drupal_static(__FUNCTION__, array());
  if (!in_array($type, array('context', 'service'), TRUE)) {
    // Abort on invalid info type.
    return;
  }

  if ($reset || !isset($info[$type])) {
    // Fetch from cache.
    if (!$reset && $cache = cache_get($cid = 'authchain:info:' . $type)) {
      $info[$type] = $cache->data;
    }
    // Rebuild info.
    else {
      $info[$type] = module_invoke_all('authchain_' . $type . '_info');
      cache_set($cid, $info[$type]);
    }
  }

  return $info[$type];
}

/**
 * Get a list of enabled services with their settings.
 *
 * @return array
 *   Service info required to execute services, keyed by name. Each value is an
 *   info array with the following key/value pairs:
 *   - 'class': Service class name.
 *   - 'settings': Setting values array.
 */
function authchain_get_active_services($reset = TRUE) {
  $service_data = &drupal_static(__FUNCTION__ . ':data');
  $services = &drupal_static(__FUNCTION__ . ':services', array());

  if ($reset || !isset($service_data)) {
    // Fetch from cache.
    if (!$reset && $cache = cache_get('authchain:services')) {
      $service_data = $cache->data;
    }
    // Rebuild active service info and settings.
    else {
      // Fetch service settings.
      $result = db_select('authchain_service', 's')
        ->fields('s', array('service', 'settings'))
        ->condition('s.active', 1)
        ->orderBy('weight')
        ->addTag('authchain_service')
        ->execute();

      // Iterate through active services and only confirm active ones.
      $info = authchain_get_info('service');
      $service_data = array();
      while ($row = $result->fetchObject()) {
        $name = $row->service;
        if (isset($info[$name]) && !empty($info[$name]['class'])) {
          $service_data[$name] = array(
            'class' => $info[$name]['class'],
            'settings' => $row->settings,
          );
        }
      }

      // Store as cache.
      cache_set('authchain:services', $service_data);
    }
  }

  // Initialize services.
  foreach ($service_data as $name => $data) {
    if (class_exists($data['class'])) {
      $services[$name] = $data['class']($data['settings']);
    }
  }

  return $services;
}

/**
 * Implements hook_hook_info().
 */
function authchain_hook_info() {
  $hooks = array();
  $hooks['authchain_context_info'] = array(
    'group' => 'authchain',
  );
  $hooks['authchain_service_info'] = array(
    'group' => 'authchain',
  );
  return $hooks;
}
